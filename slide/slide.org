#+OPTIONS: timestamp:nil toc:nil num:nil author:nil
#+TITLE: O marketing digital na atualidade
#+SUBTITLE: a relevância do marketing digital no sucesso de uma empresa
* O marketing digital na atualidade
** Introdução
   O marketing evoluiu extraordinariamente devido à globalização e o maior acesso à internet nos últimos vinte anos, essa evolução trouxe transformações significativas nos diversos segmentos mercadológicos e no comportamento humano, fazendo com que as estratégias e formas de se comunicar com um determinado público também mude.
** Objetivos gerais
   Este trabalho tem como objetivo determinar o impacto do marketing no sucesso ou fracasso da organização, levando em consideração o contexto atual e as mudanças no marketing desde o advento da internet e sua disseminação.
** Objetivos específicos
   - Identificar as mudanças e tendências de comportamento do consumidor na era digital;
   - Reconhecer os diferentes segmentos da sociedade digital;
   - Apresentar as principais estratégias do marketing digital;
   - Aferir o alcance e/ou eficiência do marketing digital;
   - Estimar os prejuízos causados e/ou oportunidades perdidas pela não utilização
** Justificativa
   A principal premissa desta pesquisa é que o marketing deve se adaptar à mutabilidade do consumidor. E devido às evoluções tecnológicas, sociais e econômicas o consumidor se voltou para os meios digitais. Seguindo a premissa proposta, o marketing acompanha o consumidor nessa mudança e em consequência dessa mudança ser nova e constante, dúvidas sobre o assunto são frequentes.
** Conteúdo
*** Conceito de marketing
*** Mudanças no marketing na era digital
*** Segmentos da sociedade digital
*** Planejamento e aplicação do marketing digital
** Análise da pesquisa
** Problematização
** Conclusão
