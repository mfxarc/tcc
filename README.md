# TCC - A relevância do marketing digital na atualidade
 |PASTA/ARQUIVO | USO                                          |
 |--------------|----------------------------------------------|
 |fontes/       | Colocar as referências bibliográficas usadas |
 |toc.tex       | Pesquisa em LaTeX                            |
 |toc.pdf       | Arquivo final da pesquisa em pdf             |
---
## PROGRESSO
- [X] Identificar as mudanças e tendências de comportamento do consumidor na era digital;
  - [X] Inclusividade
  - [X] Horizontalidade
  - [X] Individualidade
- [X] Reconhecer os diferentes segmentos da sociedade digital;
- [X] Apresentar principais métricas de desempenho;
- [X] Apresentar as principais estratégias do marketing digital;
- [X] Aferir o alcance e/ou eficiência do marketing digital;
- [X] Estimar os prejuízos causados e/ou oportunidades perdidas pela não utilização do marketing digital;

## TODO SEÇÃO - Analise dos resultados
- DICA GERAL: É tudo dentro de Marketing de Conteúdo, ou seja, conceitos como postagem, publicação, redes sociais e formas de interagir com o público de forma online em que há um certo trabalho humano envolvido.
- DICA GERAL 2: Relacione o máximo com a pesquisa
- DICA GERAL 3: Tá na dúvida, se pergunte como isso que você está fazendo explica ou se relaciona com esses dois pontos:
  - Como o marketing de conteúdo influência na escolha de uma marca?
  - Se ele influência, como ele faz isso?
---
- Relacionar o que faz uma pessoa se atrair pela marca com os dados da pesquisa
  - Dica: Halo Effect | Arquétipos e Personas no Marketing | Relação entre psicologia e marketing
- Relacionar o item anterior com o Marketing de Conteúdo.
  - De que forma uma postagem bem feita influência na escolha
  - De que forma as pessoas escolhidas em uma estratégia de Marketing de Conteúdo influência na escolha (exemplo blogueira A ou B)
- Analise dos dados sobre as redes sociais mais usadas
  - Como as redes sociais mais usadas funcionam?
    - Por que são mais usadas?
      - Como tirar proveito da estrutura dessas redes sociais em estratégias de Marketing de Conteúdo?

## TODO SEÇÃO - Considerações Finais
- Como o marketing e principalmente a estratégia de marketing de conteúdo é relevante?
  - Visão geral de como impacta atuais e possíveis consumidores
  - Demonstrações de usos na realidade
- Encaixar um "resumo" coerente sobre o trabalho

## TODO Finalização (Foco atual)
- [ ] *(A fazer)* Análise dos resultados
- [ ] *(A fazer)* Considerações finais
- [X] *(Feito)* Relatórios da pesquisa
- [X] *(Feito)* Corrigir gráficos que estão errados
- [X] *(Feito)* Resumo

## TODO Pesquisa
- [ ] *(Ludovico não fez)* Fazer mais pesquisas sobre os "Segmentos da sociedade" e expor mais dados (gráficos, tabelas e pesquisas)
- [X] **(Ludovico fez)** Apresentar as principais estratégias do marketing digital;
- [X] **(Marcos fez)** Aferir o alcance e/ou eficiência do marketing digital;
- [X] **(Marcos fez)** Metodologia
- [X] **(Marcos fez)** Planejamento e aplicação do marketing;
- [X] **(Marcos fez)** Principais métricas;
- [X] **(Marcos fez)** A prática do marketing [introdução];
- [X] **(Ayumi fez)** Estimar os prejuízos causados e/ou oportunidades perdidas pela não utilização do marketing digital;

## TODO Formatação/Organização
- [X] **(Marcos fez)** Correção nas seções
- [X] **(Marcos fez)** Referências no índice
- [X] **(Marcos fez)** Formatão modelo abnt
- [X] **(Marcos fez)** Capa, contra-capa e índice 
- [X] **(Marcos fez)** Colocar referências em bibtex para fácil manutenção
