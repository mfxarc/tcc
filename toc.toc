\boolfalse {citerequest}\boolfalse {citetracker}\boolfalse {pagetracker}\boolfalse {backtracker}\relax 
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {1}INTRODUÇÃO}{5}{}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {2}OBJETIVOS}{7}{}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.1}OBJETIVO GERAL}{7}{}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {2.2}OBJETIVOS ESPECÍFICOS}{7}{}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {3}JUSTIFICATIVA}{8}{}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {4}PROBLEMATIZAÇÃO}{8}{}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {5}REVISÃO BIBLIOGRÁFICA}{9}{}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {5.1}A PRÁTICA DO MARKETING}{9}{}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {5.2}MUDANÇAS NO MARKETING NA ERA DIGITAL}{9}{}%
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {5.2.1}A inclusividade}{10}{}%
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {5.2.2}Horizontalidade}{11}{}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {5.3}SEGMENTOS DA SOCIEDADE DIGITAL}{12}{}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {5.4}PLANEJAMENTO E APLICAÇÃO DO MARKETING}{13}{}%
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {5.4.1}Métricas de desempenho}{14}{}%
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{\numberline {5.4.1.1}Os 5as}{14}{}%
\defcounter {refsection}{0}\relax 
\contentsline {paragraph}{\numberline {5.4.1.2}Métricas CAC e CDM}{16}{}%
\defcounter {refsection}{0}\relax 
\contentsline {subsubsection}{\numberline {5.4.2}Marketing de conteúdo}{17}{}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {6}METODOLOGIA}{19}{}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {7}ANÁLISE E DISCUSSÃO DOS RESULTADOS}{20}{}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {7.1}ANÁLISE DOS RESULTADOS}{20}{}%
\defcounter {refsection}{0}\relax 
\contentsline {subsection}{\numberline {7.2}DISCUSSÃO DOS RESULTADOS}{25}{}%
\defcounter {refsection}{0}\relax 
\contentsline {section}{\numberline {8}CONSIDERAÇÕES FINAIS}{26}{}%
\contentsfinish 
